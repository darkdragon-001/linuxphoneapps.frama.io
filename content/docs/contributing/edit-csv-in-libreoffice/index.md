+++
title = "Editing CSV in LibreOffice"
description = "Adding or editing existing listings"
date = 2022-04-07T19:30:00+02:00
draft = false
weight = 40
sort_by = "weight"
template = "docs/page.html"

[extra]
toc = true
top = false
+++

### Introduction

While the overall plan is to make app listings editable directly (such as this page is by its link on the button), for maintenance purposes this has not been enabled yet. We also want third parties to build upon data MGLapps, LINMOBapps and now LinuxPhoneApps.org contributors have amassed, and for that the .csv lists have to stay.

### Opening the file in LibreOffice

CSV are a bit underspecified, therefore import options matter.

Just set them as they are set in the following screenshot:

![Import settings in LibreOffice: Only tick the checkbox on Comma](import.png)

#### Recommended settings

In theory, you can start editing now. However, it's way easier if you follow two more steps:

First, click into the B2 field and then go into the "View" menu and select the "Freeze Rows and Columns" field.

![Fixing the layout](fixing-layout.png)

If you just want to add apps, you can add it into the first empty row at the bottom of the list for apps.csv (or add it in where it would go alphabetically for games.csv). 

If you want to edit apps, finding the correct row can be annoying. Filters can help here.

Turn on auto-filters by clicking the icon or, going to the "Data" menu and select "Auto Filter" (or hit Shift+Ctrl+L if your LibreOffice is set up the way mine is).

![Filter settings in LibreOffice: Turning on auto filters](filter-1.png)

If you hit one of the little arrows in the top columns a menu will come up that allows you to search the content of that column. With the A column that means you're filtering through app names: E.g. if you type Amberol, and hit OK, you'll be editing the Amberol entry and only that one.

![Using the filter to narrow down](filter-2.png)

You can also scroll through the filter list and select a few apps if you e.g want to edit a few similar apps at once. After you've selected what you want to edit and hit done, you'll only see the app(s) you've selected and can now edit them without accidentally drive-by editing the wrong column.

![Results of filtering](filter-3.png)

There's one issue left: _Typographic Quotes._ We need non-typographic quotes for quotations in the summary and description fiels. If you rarely edit for LinuxPhoneApps, just copy paste a non-typographic quote and paste it four times in these two fields. Otherwise, you can go to "Tools" and select "AutoCorrect Options". Then select "Localized options" and uncheck the checkboxes below "Single Quotes" and "Double Quotes". Hit OK, and you're done :)

![AutoCorrect Options](autocorrect-options.png)

### Alternatively: Add apps with Vim

You can also edit with Vim. While in normal mode and on the first line, hit `yy` to copy the line that specifies the contents of each column.
After that, navigate to the bottom of the file by hitting `L`, (or the navigate to the alphabetical position for games.csv), and then paste the line once or twice by hitting `p`. Now you just need to replace the words by the appropriate contents for the app you want to add with vim. Depending on whether you pasted a helper line, delete that again before saving (:w) by hitting `dd`.

#### Fields and contents

No matter whether you edit with LibreOffice or not, here's what kind of contents is supposed to go into the different columns:

| field      | value / form | note |
|------------|--------------|------|
| name       | name | try to write it as it's written on its project page |
| repository             | URL        | **mandatory** |
| website                | URL        | optional |
| bugtracker             | URL        | **only fill if not Gitea/GitHub/GitLab issues** |
| donations              | URL        | optional, link to donations |
| translations           | URL        | optional, link to translation project |
| more information       | URLs       | helpful links seperated by space |
| license                | identifier | [SPDX 3 identifier](https://spdx.org/licenses/),**mandatory** |
| metadata license                | identifier | [SPDX 3 identifier](https://spdx.org/licenses/), optional |
| category                        | value           | [check existing values](https://linuxphoneapps.org/categories), if multiple seperate by comma |
| summary                         | text                | **mandatory**, keep it short |
| summary source                  | url or no quotation | **mandatory**
| descritption                    | usually a quote, markdown | should explain the app well |
| descritption source             | url or no quotation | **mandatory** if description is filled|
| screenshots                     | url             | if multiple seperate by space |
| mobile compatibility            | number          | [check existing values](https://linuxphoneapps.org/mobile-compatibility/), **mandatory** |
| status                          | value           | [check existing values](https://linuxphoneapps.org/status/), if multiple seperate by comma |
| notice                          | text, markdown  | additional facts regarding the app, explaining shortcomings or features |
| framework                       | value           | [check existing values](https://linuxphoneapps.org/frameworks/), if multiple seperate by comma |
| backend                         | value           | [check existing values](https://linuxphoneapps.org/backends/), if multiple seperate by comma |
| service                         | value           | [check existing values](https://linuxphoneapps.org/services/), if multiple seperate by comma |
| appid                           | value           | e.g. org.gnome.GHex |
| scale-to-fit                    | value           | often the app id, [details](https://linmob.net/pinephone-setup-scaling-in-phosh/) |
| flatpak                         | URL             | flatpak URL, often flathub URL |
| repology                        | package term    | find on repology.org, if multiple seperate by space |
| aur                             | package name(s) | if multiple seperate by space |
| postmarketos                    | package name(s) | if multiple seperate by space |
| debian                          | package name(s) | if multiple seperate by space | 
| appstream link                  | appstream://appid | only fill when there's an appid |
| appdata.xml or metainfo.xml url | URL             | choose the raw/plain-text option |
| freedesktop categories          | value,value     | see [1](https://specifications.freedesktop.org/menu-spec/latest/apa.html), [2](https://specifications.freedesktop.org/menu-spec/latest/apas02.html) for values  |
| main languages                  | value           | comma separated for multiple |
| build system                    | value           | comma separated for multiple |     
| date added                      | yyyy-mm-dd      | **mandatory**                    |
| original reporter               | your (nick)name | **mandatory**                    |
| updated                         | yyyy-mm-dd      | refers to updating reviewing the listing, only fill when updating an existing listing     |
| updated by                      | your (nick)name | only fill when updating      |
