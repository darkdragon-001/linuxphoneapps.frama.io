+++
title = "Mobile Compatibility: Needs testing"
description = "Needs evaluation"
date = 2021-08-15T08:50:45+00:00
updated = 2021-08-15T08:50:45+00:00
draft = false
+++
Apps listed here were added early on, but never got a proper rating. Likely the app could not be evaluated due to not being able to try it at all (e.g. the app is unmaintained or archived, and nobody managed to build it) or due to not being able to evaluate it properly, e.g. when an app requires certain hardware or a certain software setup in order to evaluate it fully.

Please help out if you can!